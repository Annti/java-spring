package com.tutorialspoint;


import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class MainApp {
    public static void main(String[] args) {
        //kontekst aplikacji
        //ładujemy plik Beans.xml
        //który zawiera obiekty które powinny być zainicjalizowane
        AbstractApplicationContext context =
                new ClassPathXmlApplicationContext("Beans.xml");
        // tworzymy obiekt kontkstu korzystając z metody getBean
        // teraz będę mogła odwołać się do dowolnej metody z kalsy kontekstu
        HelloWorld obj = (HelloWorld) context.getBean("helloWorld");

        obj.getMessage1();
        obj.getMessage2();

        HelloPoland objB = (HelloPoland) context.getBean("helloPoland");
        objB.getMessage1();
        objB.getMessage2();
        objB.getMessage3();
        //init things

        //this way is better (way with Beans.xml)
        ExampleBean initex = (ExampleBean) context.getBean("initExampleBean");
        System.out.println("Init object : " + initex.getInitObj());

        // secod way
        ExampleBeanImplementInterface initexImplements = new ExampleBeanImplementInterface();
        initexImplements.afterPropertiesSet();
        System.out.println("Init object : " + initexImplements.getInitObject());



        TextEditor te = (TextEditor) context.getBean("textEditor");
        te.spellCheck();


        JavaCollection jc=(JavaCollection)context.getBean("javaCollection");

        jc.getAddressList();
        jc.getAddressSet();
        jc.getAddressMap();
        jc.getAddressProp();


        context.registerShutdownHook();
    }
}