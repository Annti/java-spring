package com.tutorialspoint;

/**
 * Created by Annti on 2016-04-27.
 */
public class ExampleBean {


    private String initObj;

    public void init() {
        // do some initialization work
        initObj = " Init this examplebean is here !!!";
    }

    public void destroy(){
        initObj = "Object destroy";
        System.out.println(initObj);
    }

    public String getInitObj() {
        return initObj;
    }

    public void setInitObj(String initObj) {
        this.initObj = initObj;
    }


}
