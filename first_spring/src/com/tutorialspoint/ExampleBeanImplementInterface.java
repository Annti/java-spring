package com.tutorialspoint;

import org.springframework.beans.factory.InitializingBean;

/**
 * Created by Annti on 2016-04-27.
 */
//it's first example for initializing bean
public class ExampleBeanImplementInterface implements InitializingBean {

    private String initObject;
//    public ExampleBeanImplementInterface(){}
    public void afterPropertiesSet() {
        // do some initialization work
        initObject = " Object implements InitializingBean is here !!!";
    }

    public String getInitObject() {
        return initObject;
    }

    public void setInitObject(String initObject) {
        this.initObject = initObject;
    }
}
