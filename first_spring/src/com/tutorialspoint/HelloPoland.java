package com.tutorialspoint;

/**
 * Created by aneta on 08.05.16.
 */
public class HelloPoland {
    private String message1;
    private String message2;
    private String message3;

    public void setMessage1(String message){
        this.message1  = message;
    }

    public void setMessage2(String message){
        this.message2  = message;
    }

    public void setMessage3(String message){
        this.message3  = message;
    }

    public void getMessage1(){
        System.out.println("Poland Message1 : " + message1);
    }

    public void getMessage2(){
        System.out.println("Poland Message2 : " + message2);
    }

    public void getMessage3(){
        System.out.println("Poland Message3 : " + message3);
    }

    public void init(){
        System.out.println("Bean is going through init.");
    }
    public void destroy(){
        System.out.println("Bean will destroy now.");
    }
}
