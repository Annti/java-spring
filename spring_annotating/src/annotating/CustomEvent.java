package annotating;

import org.springframework.context.ApplicationEvent;

/**
 * Created by aneta on 12.05.16.
 */
public class CustomEvent extends ApplicationEvent {

    public CustomEvent(Object source) {
        super(source);
    }

    public String toString(){
        return "My Custom Event";
    }
}