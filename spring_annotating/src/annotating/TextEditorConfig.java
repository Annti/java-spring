package annotating; /**
 * Created by aneta on 09.05.16.
 */
import org.springframework.context.annotation.*;

@Configuration
public class TextEditorConfig {

    @Bean
    public TextEditor textEditor(){
        return new TextEditor( spellChecker() );
    }

    @Bean
    public SpellChecker spellChecker(){
        return new SpellChecker( );
    }
}