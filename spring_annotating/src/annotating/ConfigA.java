package annotating;

import annotating.A;
import org.springframework.context.annotation.*;

/**
 * Created by aneta on 09.05.16.
 */
@Configuration
public class ConfigA {
    @Bean(initMethod = "init", destroyMethod = "cleanup" )
    public A a() {
        return new A();
    }
}
