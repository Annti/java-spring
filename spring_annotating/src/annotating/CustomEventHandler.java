package annotating;

/**
 * Created by aneta on 12.05.16.
 */
import org.springframework.context.ApplicationListener;

public class CustomEventHandler
        implements ApplicationListener<CustomEvent>{

    public void onApplicationEvent(CustomEvent event) {
        System.out.println(event.toString());
    }

}