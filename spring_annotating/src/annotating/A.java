package annotating;

/**
 * Created by aneta on 09.05.16.
 */
public class A {

    public A(){
        System.out.println("create annotating.A");
    }

    public void printA(){
        System.out.println("Print AAAAAAAAAAAAA");
    }

    public void init() {
        System.out.println("init annotating.A");
    }
    public void cleanup() {
        System.out.println("cleanup annotating.A");
    }
}
