package annotating;

import org.springframework.context.annotation.*;

/**
 * Created by aneta on 09.05.16.
 */
@Configuration
@Import(ConfigA.class)
public class ConfigB {

    @Bean
    @Scope("prototype")
    B b(){
        return new B();
    }
}