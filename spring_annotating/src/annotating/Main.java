package annotating; /**
 * Created by aneta on 09.05.16.
 */
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {


        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(annotating.HelloWorldConfig.class);

        annotating.HelloWorld helloWorld = ctx.getBean(annotating.HelloWorld.class);

        helloWorld.setMessage("Hello World!");
        helloWorld.getMessage();

        ApplicationContext ctx2 =
                new AnnotationConfigApplicationContext(annotating.TextEditorConfig.class);

        annotating.TextEditor te = ctx2.getBean(annotating.TextEditor.class);

        te.spellCheck();

        AbstractApplicationContext ctx3 =
                new AnnotationConfigApplicationContext(annotating.ConfigB.class);
        // now both beans annotating.A and annotating.B will be available...
        annotating.A a = ctx3.getBean(annotating.A.class);
        a.printA();
        System.out.println("przerwa");
        annotating.B b = ctx3.getBean(annotating.B.class);
        b.printB();
        ctx3.registerShutdownHook();

        ConfigurableApplicationContext context =
                new ClassPathXmlApplicationContext("Beans.xml");

        // Let us raise a start event.
        context.start();

        HelloWorld obj = (HelloWorld) context.getBean("helloWorld");

        obj.getMessage();

        // Let us raise a stop event.
        context.stop();


        ConfigurableApplicationContext context2=
                new ClassPathXmlApplicationContext("Beans.xml");

        CustomEventPublisher cvp =
                (CustomEventPublisher) context2.getBean("customEventPublisher");
        cvp.publish();
        cvp.publish();
    }
}
